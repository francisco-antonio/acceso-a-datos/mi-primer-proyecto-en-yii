<!DOCTYPE html>
<html xml:lang="es" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="es" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
 
        <!--jQuery 2.1-->
        <script src="<?php echo Yii::app()->request->baseUrl;?> /js/jquery.min.js"></script>
         
        <!--Twitter Bootstrap 3-->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?> /bootstrap/css/bootstrap.yeti.min.css">
         
        <script src="<?php echo Yii::app()->request->baseUrl; ?>
        /bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container" id="page">
        <nav class="navbar navbar-default" role="navigation">
                <!--Menú de navegación-->
        </nav>
 
        <?php
            //Cargamos el contenido o vista
            echo $content;
        ?>
 
        <div class="clear"></div>
 
        <div id="footer" class="alert alert-info">
            Copyright &copy; <?php echo date('Y'); ?> by My Company.
            All Rights Reserved.
            <?php echo Yii::powered(); ?>
        </div>
    </div>
</body>
</html>

